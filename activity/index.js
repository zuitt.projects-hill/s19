let student1 = {
	name: "Shawn Michaels",
	birthday: "May 5, 2003",
	age: 18,
	isEnrolled: true,
	classes: ["Philosphy 101", "Social Sciences 201"]
}

let student2 = {
	name: "Steve Austin",
	birthday: "June 15, 2001",
	age: 20,
	isEnrolled: true,
	classes: ["Philosphy 401", "Natural Sciences 402"]
}


function introduce(student){

	//Note: You can destructure objects inside functions.
	const {name, birthday, age, isEnrolled, classes} = student1

	console.log(`Hi! I'm ${name}. I am ${age} years old.`);
	console.log(`I study the following courses ${classes}`);
}

const getCube = (num) => (num ** 3);

let cube = getCube(3);

console.log(cube);


let numArr = [15,16,32,21,21,2]

const [num1, num2, num3, num4, num5, num6] = numArr;

numArr.forEach(num => {

	console.log(num);
})

let numSquared = numArr.map(num => num ** 2);

console.log(numSquared);

	class dog {
		constructor(name, breed, age){
			this.name = name;
			this.breed = breed;
			this.dogAge = age *7;

		}
	};

	let dog1 = new dog("Chiso", "Golden Retriever", 0.5);
	let dog2 = new dog("Puchi", "Alaskan Malamute", 0.5);

	console.log(dog1);
	console.log(dog2);
